package net.apisp.quick.server;

import java.io.IOException;

public interface ResponseExecutor {
	void response() throws IOException;
}
